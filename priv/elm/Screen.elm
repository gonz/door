module Screen exposing (..)

import Html exposing (..)
import Html.App as App
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Json.Decode
import WebSocket
import WsMessages
import Style


listenServer model =
    "ws://" ++ model.host ++ "/websocket"


main =
    App.programWithFlags
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


type alias Flags =
    { host : String
    , path : String
    }


init : Flags -> ( Model, Cmd a )
init flags =
    let
        initModel =
            { loggedIn = False
            , loginInput = ""
            , id = ""
            , chatInput = ""
            , activeChannel = "lobby"
            , messages = []
            , host = flags.host
            , path = flags.path
            , channelInput = ""
            , channels = [ "lobby" ]
            }
    in
        ( initModel, Cmd.none )



-- MODEL


type alias ID =
    String


type alias Channel =
    String


type alias MessageString =
    String


type alias Message =
    String


type alias Model =
    WsMessages.Model



-- UPDATE


type Msg
    = LoginInput String
    | SendLogin
    | NewMessage String
    | ChatInput String
    | SendChat
    | ScrollHeight
    | ChannelInput String
    | JoinChannelRequest
    | SwitchChannel String
    | NoOp


update msg model =
    case msg of
        LoginInput str ->
            ( { model | loginInput = str }, Cmd.none )

        SendLogin ->
            ( { model
                | loginInput = ""
                , id = model.loginInput
              }
            , WebSocket.send (listenServer model)
                (WsMessages.encodeLogin model)
            )

        ChatInput str ->
            ( { model | chatInput = str }, Cmd.none )

        SendChat ->
            WsMessages.handleOutgoingMessage model

        NewMessage str ->
            WsMessages.handleMessage model str

        ScrollHeight ->
            ( model, WsMessages.scrollHeight "chat-window" )

        ChannelInput str ->
            ( { model | channelInput = str }, Cmd.none )

        JoinChannelRequest ->
            let
                channel =
                    model.channelInput
            in
                ( { model
                    | channelInput = ""
                    , channels = model.channels ++ [ channel ]
                  }
                , (WebSocket.send (listenServer model)
                    (WsMessages.encodeChannelJoinRequest model.id channel)
                  )
                )

        SwitchChannel channel ->
            ( { model | activeChannel = channel }
            , WsMessages.scrollHeight "chat-window"
            )

        NoOp ->
            ( model, Cmd.none )



-- SUBSCRIPTIONS


subscriptions model =
    WebSocket.listen (listenServer model) NewMessage



-- VIEW


loginField loggedIn loginInput =
    input
        [ onInput LoginInput
        , value loginInput
        , placeholder "Username"
        , autofocus True
        , hidden loggedIn
        ]
        []


loginButton loggedIn =
    button
        [ onClick SendLogin, hidden loggedIn ]
        [ text "Login" ]


chatInput inputText =
    input
        [ onInput ChatInput
        , value inputText
        , placeholder "Write messages here"
        , autofocus True
        , Style.chatInputStyle
        , onEnter SendChat
        ]
        []


sendButton =
    button
        [ onClick SendChat ]
        [ text "Send" ]


showMessage msg =
    case msg of
        WsMessages.ChatNotification id channel messageString ->
            div []
                [ text ("(" ++ channel ++ ")" ++ " " ++ id ++ ": " ++ messageString) ]

        WsMessages.JoinChannelNotification id channel ->
            div []
                [ text (id ++ " joined channel " ++ channel) ]

        WsMessages.LeaveChannelNotification id channel ->
            div []
                [ text (id ++ " left channel " ++ channel) ]


isActiveMessage : Channel -> WsMessages.NotificationMessage -> Bool
isActiveMessage channel message =
    case message of
        WsMessages.ChatNotification _ messageChannel _ ->
            channel == messageChannel

        WsMessages.JoinChannelNotification _ messageChannel ->
            channel == messageChannel

        WsMessages.LeaveChannelNotification _ messageChannel ->
            channel == messageChannel


onEnter msg =
    let
        tagger code =
            if code == 13 then
                msg
            else
                NoOp
    in
        on "keydown" (Json.Decode.map tagger keyCode)


messageDiv activeChannel messages =
    let
        activeMessages =
            List.filter (isActiveMessage activeChannel) messages
    in
        div
            [ id "chat-window"
            , Style.chatWindowStyle
            ]
            (List.map showMessage activeMessages)


channelInput inputValue =
    input
        [ onEnter JoinChannelRequest
        , onInput ChannelInput
        , placeholder "Join channel"
        , value inputValue
        , Style.textFieldStyle
        ]
        []


channelJoinButton =
    button
        [ onClick JoinChannelRequest
        , style [ Style.bigFieldFont ]
        ]
        [ text "Join" ]


channelButton activeChannel channel =
    let
        activeChannelStyle =
            style [ ( "font-weight", "bold" ) ]
    in
        if activeChannel == channel then
            button [ activeChannelStyle ] [ text channel ]
        else
            button [ onClick (SwitchChannel channel) ] [ text channel ]


channelList channels activeChannel =
    let
        buttonList =
            List.map (channelButton activeChannel) channels
    in
        div [] buttonList


currentChannelTitle channel =
    div [] [ h4 [] [ text ("Current channel: " ++ channel) ] ]


view model =
    div []
        [ loginField model.loggedIn model.loginInput
        , loginButton model.loggedIn
        , div
            [ hidden (not model.loggedIn) ]
            [ channelInput model.channelInput
            , channelJoinButton
            , channelList model.channels model.activeChannel
            , messageDiv model.activeChannel model.messages
            , chatInput model.chatInput
            ]
        ]
