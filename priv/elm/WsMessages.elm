port module WsMessages exposing (..)

import Json.Encode exposing (encode, string, int, object)
import Json.Decode exposing ((:=), decodeString, object2, object3, Decoder)
import WebSocket


type alias ID =
    String


type alias Channel =
    String


type alias MessageString =
    String


type EventMessage
    = LoginSuccess String
    | ChatMessage ID MessageString Channel
    | JoinChannelMessage ID Channel
    | LeaveChannelMessage ID Channel
    | Error String


encodeLogin { loginInput } =
    encode 0
        (object
            [ ( "message_type", string "login" )
            , ( "id", string loginInput )
            ]
        )


encodeChatMessage { id, chatInput, activeChannel } =
    encode 0
        (object
            [ ( "message_type", string "chat_message" )
            , ( "id", string id )
            , ( "channel", string activeChannel )
            , ( "message", string chatInput )
            ]
        )


encodeChannelJoinRequest id channel =
    encode 0
        (object
            [ ( "message_type", string "channel_join_request" )
            , ( "id", string id )
            , ( "channel", string channel )
            ]
        )


encodeChannelLeaveRequest id channel =
    encode 0
        (object
            [ ( "message_type", string "channel_leave_request" )
            , ( "id", string id )
            , ( "channel", string channel )
            ]
        )


decodeLoginSuccess msg =
    case decodeString ("id" := Json.Decode.string) msg of
        Ok id ->
            LoginSuccess id

        Err error ->
            Error error


decodeChatMessage msg =
    let
        decodeFun =
            (decodeString
                (object3 (,,)
                    ("id" := Json.Decode.string)
                    ("channel" := Json.Decode.string)
                    ("message" := Json.Decode.string)
                )
                msg
            )
    in
        case decodeFun of
            Ok ( id, channel, message ) ->
                ChatMessage id channel message

            Err error ->
                Error error


decodeJoinChannelMessage msg =
    let
        decodeFun =
            (decodeString
                (object2 (,)
                    ("id" := Json.Decode.string)
                    ("channel" := Json.Decode.string)
                )
                msg
            )
    in
        case decodeFun of
            Ok ( id, channel ) ->
                JoinChannelMessage id channel

            Err error ->
                Error error


decodeLeaveChannelMessage msg =
    let
        decodeFun =
            (decodeString
                (object2 (,)
                    ("id" := Json.Decode.string)
                    ("channel" := Json.Decode.string)
                )
                msg
            )
    in
        case decodeFun of
            Ok ( id, channel ) ->
                LeaveChannelMessage id channel

            Err error ->
                Error error


apply : Decoder (a -> b) -> Decoder a -> Decoder b
apply func value =
    object2 (<|) func value


decodeMessage msg =
    case decodeString ("message_type" := Json.Decode.string) msg of
        Ok "login_success" ->
            decodeLoginSuccess msg

        Ok "chat_message" ->
            decodeChatMessage msg

        Ok "join_channel" ->
            decodeJoinChannelMessage msg

        Ok "leave_channel" ->
            decodeLeaveChannelMessage msg

        Ok _ ->
            Error "unknown type"

        Err error ->
            Error error


listenServer model =
    "ws://" ++ model.host ++ "/websocket"


handleOutgoingMessage model =
    let
        channels =
            model.channels

        activeChannel =
            model.activeChannel

        message =
            model.chatInput

        id =
            model.id
    in
        case message of
            "!leave" ->
                let
                    newChannels =
                        List.filter (\c -> c /= activeChannel) channels

                    newActiveChannel =
                        case List.head newChannels of
                            Just c ->
                                c

                            Nothing ->
                                "lobby"
                in
                    ( { model
                        | channels = newChannels
                        , activeChannel = newActiveChannel
                      }
                    , WebSocket.send (listenServer model)
                        (encodeChannelLeaveRequest id activeChannel)
                    )

            _ ->
                ( { model | chatInput = "" }
                , WebSocket.send (listenServer model)
                    (encodeChatMessage model)
                )


port scrollHeight : String -> Cmd msg


type NotificationMessage
    = ChatNotification ID MessageString Channel
    | JoinChannelNotification ID Channel
    | LeaveChannelNotification ID Channel


type alias Model =
    { loggedIn : Bool
    , loginInput : String
    , id : String
    , chatInput : String
    , activeChannel : String
    , messages : List NotificationMessage
    , host : String
    , path : String
    , channelInput : String
    , channels : List String
    }


type alias Message =
    String


handleMessage : Model -> Message -> ( Model, Cmd a )
handleMessage model str =
    case decodeMessage str of
        Error err ->
            ( model, Cmd.none )

        LoginSuccess id ->
            ( { model | loggedIn = True }, Cmd.none )

        ChatMessage id messageString channel ->
            ( { model
                | messages =
                    model.messages ++ [ (ChatNotification id messageString channel) ]
              }
            , scrollHeight "chat-window"
            )

        JoinChannelMessage id channel ->
            ( { model
                | messages =
                    model.messages ++ [ (JoinChannelNotification id channel) ]
              }
            , scrollHeight "chat-window"
            )

        LeaveChannelMessage id channel ->
            ( { model
                | messages =
                    model.messages ++ [ (LeaveChannelNotification id channel) ]
              }
            , scrollHeight "chat-window"
            )
