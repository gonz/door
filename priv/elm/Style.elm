module Style exposing (..)

import Html.Attributes exposing (style)


bigFieldFont =
    ( "font-size", "18" )


chatInputStyle =
    style [ ( "width", "80%" ), bigFieldFont ]


chatWindowStyle =
    style
        [ ( "width", "100%" )
        , ( "height", "80%" )
        , ( "overflow", "scroll" )
        , ( "overflow-x", "hidden" )
        , ( "overflow-y", "auto" )
        , ( "word-wrap", "break-word" )
        , ( "font-size", "18" )
        ]


textFieldStyle =
    style [ bigFieldFont ]
