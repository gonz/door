-module(door_user).

-behaviour(gen_server).

%% API functions
-export([start_link/1]).

%% gen_server callbacks
-export([init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3]).

-record(state, {id = <<"">> :: binary()}).

%%%===================================================================
%%% API functions
%%%===================================================================
start_link(ID) ->
    gen_server:start_link(?MODULE, [ID], []).

id(Pid) ->
    gen_server:call(Pid, id).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

init([ID]) ->
    syn:register({user, ID}, self()),
    syn:join(users, self()),
    {ok, #state{id = ID}}.

handle_call(id, _From, S) ->
    {reply, S#state.id, S};
handle_call(Req, From, #state{id = ID} = S) ->
    Reply = ok,
    lager:info("Received unknown call from ~p (ID '~p'): ~p~n",
               [ID, From, Req]),
    {reply, Reply, S}.

handle_cast(Msg, #state{id = ID} = S) ->
    lager:info("Received unknown cast (ID '~p'): ~p~n", [ID, Msg]),
    {noreply, S}.

handle_info(Info, #state{id = ID} = S) ->
    lager:info("Received unknown info (ID '~p'): ~p~n", [ID, Info]),
    {noreply, S}.

terminate(_Reason, _S) ->
    ok.

code_change(_OldVsn, S, _Extra) ->
    {ok, S}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
