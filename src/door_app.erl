%%%-------------------------------------------------------------------
%% @doc door public API
%% @end
%%%-------------------------------------------------------------------

-module(door_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%%====================================================================
%% API
%%====================================================================

start(_StartType, _StartArgs) ->
    syn:init(),

    Dispatch = cowboy_router:compile(
                 [{'_', [{"/static/[...]", cowboy_static, {priv_dir, door, "static"}},
                         {"/", cowboy_static, {priv_file, door, "index.html"}},
                         {"/websocket", ws_handler, []}
                         ]
                  }
                 ]
                ),
    cowboy:start_http(http, 100, [{port, 8080}],
                      [{env, [{dispatch, Dispatch}]}]
                     ),

    door_sup:start_link().

%%--------------------------------------------------------------------
stop(_State) ->
    ok.

%%====================================================================
%% Internal functions
%%====================================================================
