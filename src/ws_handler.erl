-module(ws_handler).

-export([init/3, websocket_init/3,
         websocket_handle/3, websocket_info/3,
         websocket_terminate/3]).

-record(state, {id = <<"">> :: binary(),
                channels = [<<"lobby">>] :: [binary()]}).

init(_, _Req, _Opts) ->
    {upgrade, protocol, cowboy_websocket}.

websocket_init(_Type, Req, _Opts) ->
    lager:info("User connected"),
    syn:join(users, self()),
    {ok, Req, #state{}}.

websocket_handle({text, Data}, Req, OldState) ->
    handle_data(Data, Req, OldState);
websocket_handle(Data, Req, State) ->
    lager:info("Handle data '~p'~n", [Data]),
    {ok, Req, State}.

websocket_info({leave_channel, ID, Channel}, Req, OldState) ->
    {reply, {text, leave_channel_message(ID, Channel)}, Req, OldState};
websocket_info({join_channel, ID, Channel}, Req, OldState) ->
    {reply, {text, join_channel_message(ID, Channel)}, Req, OldState};
websocket_info(Info, Req, State) ->
    handle_info(jsx:decode(Info, [return_maps]), Req, State).

websocket_terminate(Reason, _Req, #state{id = ID, channels = Channels}) ->
    lager:info("Terminate reason: ~p~n", [Reason]),
    lists:foreach(fun(Channel) ->
                          door:leave_channel(ID, Channel)
                  end, Channels),
    ok.

handle_data(Data, Req, OldState) when is_binary(Data) ->
    Decoded = jsx:decode(Data, [return_maps]),
    handle_data(Decoded, Req, OldState);
handle_data(#{<<"message_type">> := <<"login">>, <<"id">> := ID},
            Req,
            OldState) ->
    syn:register({user, ID}, self()),
    door:join_channel(ID, <<"lobby">>),
    {reply, {text, successful_login(ID)}, Req, OldState#state{id = ID}};
handle_data(#{<<"message_type">> := <<"channel_join_request">>,
              <<"id">> := ID, <<"channel">> := Channel},
            Req, #state{channels = Channels} = OldState) ->
    door:join_channel(ID, Channel),
    {ok, Req, OldState#state{channels = [Channel | Channels]}};
handle_data(#{<<"message_type">> := <<"channel_leave_request">>,
              <<"id">> := ID, <<"channel">> := Channel},
            Req, #state{channels = Channels} = OldState) ->
    door:leave_channel(ID, Channel),
    {ok, Req, OldState#state{channels = Channels -- [Channel]}};
handle_data(#{<<"message_type">> := <<"chat_message">>, <<"id">> := ID,
              <<"message">> := Message, <<"channel">> := Channel},
            Req,
            OldState) ->
    door:message_channel(Channel, chat_message(ID, Channel, Message)),
    {ok, Req, OldState}.

handle_info(#{<<"message_type">> := <<"chat_message">>, <<"id">> := _ID,
              <<"channel">> := _Channel, <<"message">> := _Message} = Data,
            Req,
            OldState) ->
    {reply, {text, jsx:encode(maps:to_list(Data))}, Req, OldState};
handle_info(Data, Req, OldState) ->
    lager:info("Unknown info data: ~p~n", [Data]),
    {ok, Req, OldState}.

successful_login(ID) ->
    jsx:encode([{message_type, login_success}, {id, ID}]).

chat_message(ID, Channel, Message) ->
    jsx:encode([{message_type, chat_message}, {id, ID},
                {channel, Channel}, {message, Message}]).

join_channel_message(ID, Channel) ->
    Msg = [{message_type, join_channel}, {id, ID}, {channel, Channel}],
    jsx:encode(Msg).

leave_channel_message(ID, Channel) ->
    Msg = [{message_type, leave_channel}, {id, ID}, {channel, Channel}],
    jsx:encode(Msg).
