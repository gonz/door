-module(door).

%% Exports

% listening/unlistening to users
-export([listen/1, unlisten/1,
         listen/2, unlisten/2,
         listeners/1]).

% interaction with users
-export([message_id/2, message_channel/2]).

% channels
-export([join_channel/2, join_channels/2,
         leave_channel/2, channel_members/1]).

%% API

% Listening
listen(ID) ->
    syn:join({listeners, ID}, self()).

listen(ID, Pid) ->
    syn:join({listeners, ID}, Pid).

unlisten(ID) ->
    syn:leave({listeners, ID}, self()).

unlisten(ID, Pid) ->
    syn:leave({listeners, ID}, Pid).

listeners(ID) ->
    syn:get_members({listeners, ID}).

% Messaging
message_id(ID, Msg) ->
    syn:publish({listeners, ID}, Msg).

message_channel(Channel, Msg) ->
    syn:publish({channel, Channel}, Msg).

% Channels
join_channel(UserIDs, Channel) when is_list(UserIDs) ->
    lists:foreach(fun(UID) ->
                          Pid = syn:find_by_key({user, UID}),
                          syn:join({channel, Channel}, Pid)
                  end,
                  UserIDs);
join_channel(UserID, Channel) ->
    Pid = syn:find_by_key({user, UserID}),
    notify_join(UserID, Channel),
    syn:join({channel, Channel}, Pid).

join_channels(UserID, Channels) when is_list(Channels) ->
    Pid = syn:find_by_key({user, UserID}),
    lists:foreach(fun(Channel) ->
                          syn:join({channel, Channel}, Pid),
                          notify_join(UserID, Channel)
                  end,
                  Channels).

leave_channel(UserID, Channel) ->
    Pid = syn:find_by_key({user, UserID}),
    syn:leave({channel, Channel}, Pid),
    notify_leave(UserID, Channel).

channel_members(Channel) ->
    syn:get_members({channel, Channel}).

% Internals
notify_join(UserID, Channel) ->
    syn:publish({channel, Channel}, {join_channel, UserID, Channel}).

notify_leave(UserID, Channel) ->
    syn:publish({channel, Channel}, {leave_channel, UserID, Channel}).
